#!/bin/bash

# --------------------
# Script básico para iniciar serviços do Docker
# by iLeonardo Carvalho v1.0
# httpd | adaptado
# ----------

# Permissões
chown httpd:httpd -R /var/www/html
chmod go-rwx /var/www/html
chmod go+x /var/www/html
chgrp httpd -R /var/www/html
chmod go-rwx -R /var/www/html
chmod g+rx -R /var/www/html
chmod g+rwx -R /var/www/html

# Verificar se existe algum processo do Apache Web Server Rodando
service httpd stop

# Execução
exec service httpd start_docker
