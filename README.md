![Apache Logo](https://www.apache.org/foundation/press/kit/asf_logo.png)




Docker HTTPD/Source
===================

Este container contém o source do **Apache Web HTTPD** e é compilado através do **make && make install** usando a base original do Desenvolvedor.

----------



[TOC]

### Requisitos

Foi utilizado a versão **Release** acessado pelo site [Oficial do Apache](http://ftp.unicamp.br/pub/apache/) no dia 12 de Outubro de 2016.

> **Notas:**

> - [HTTPD](#HTTPD): v2.4.23
> - [APR](#APR): v1.5.2
> - [APR-UTIL](#APR-UTIL): v1.5.4
> - [APR-ICONV](#APR_ICONV): v1.2.1

> **Obs.:**

> - Baseado no SO Debian/Ubuntu
> - LATEST (últimas atualizações de acordo com o acesso)
> - Comentado e adaptado ao LSB (Não testado o systemd)

#### Estrutura

**SERVER ROOT**
```
/:.
|
\---usr
 	|
 	\---local
 		|
 		\---apache2
 			|
 			+\	...
```
**DOCUMENT ROOT**
```
/:.
|
\---var
 	|
 	\---www
 		|
 		\---html
 			|
 			|	index.html
```
**DEFAULT LAYOUT**
```
Apache Config file 		:: 	/usr/local/apache2/conf/httpd.conf
Other Config files 		:: 	/usr/local/apache2/conf/extra/
SSL Config file 		:: 	/usr/local/apache2/conf/extra/httpd-ssl.conf
ErrorLog 				:: 	/var/log/httpd/error.log
AccessLog 				:: 	/var/log/httpd/access.log
cgi-bin 				:: 	/usr/local/apache2/cgi-bin
 							(enabled by default, but some of the bundled scripts are 644)
binaries (apachectl) 	:: 	/usr/local/apache2/bin/
start/stop 				:: 	/usr/local/apache2/bin/apachectl
 							(start|restart|graceful|graceful-stop|stop|configtest|-D FOREGROUND)

# PORTAS
80 						:: 	Apache Web default
443 					:: 	Apache Web SSL (Certificado Digital)
```
> **Obs.:** Verifique o arquivo [envvars](#envvars), foi baseado nestas variaveis mas ainda não está automático, futuralmente estas variaveis estará junto com **Apache Config file**.

----------


Comandos Básicos
-------------------

#### Uso

Utilize:

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/httpd .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/httpd
```

Para Executar:

```
docker run -d -p 80:80 -p 443:443 ileonardo/httpd
```

Mas se preferir apontar o **DOCUMENT ROOT** no **HOST** do que usar do **Container**, utilize:

```
docker run -dit --name apache-app --publish=80:80 -v "/home/aluno/public":/var/www/html/ ileonardo/httpd
```

Ou usando **$PWD** (apontando local atual):

```
docker run -dit --name apache-app --publish=80:80 -v "$PWD":/var/www/html/ ileonardo/httpd
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi $(docker images -f "dangling=true" -q)
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
# Unix
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host

# Windows
docker cp <ID_Container>:/caminho/no/container/arquivo c:\caminho\no\host
```

Copiar um arquivo do **HOST** para o Container:

```
# Unix
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container

# Windows
docker cp c:\caminho\no\host\arquivo <ID_Container>:/caminho/no/container
```

Obter informações do container (PATH, STATUS, IP):

```
docker inspect <ID_Container>
```

Docker Toolbox:

```
docker-machine ls
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^httpd]: A instalação Source das dependências foram baseadas no site apontado pelo [apache](http://ftp.unicamp.br/pub/apache/).
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).