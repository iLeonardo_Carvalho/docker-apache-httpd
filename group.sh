#!/bin/bash

if grep -q httpd /etc/group; then
	echo "Grupo httpd Existente, Configurando o Usuário do Apache..."
	useradd httpd -g httpd -d /dev/null -s /sbin/nologin
else
	echo "Grupo httpd não Existe, Configurando..."
	groupadd httpd
	useradd httpd -g httpd -d /dev/null -s /sbin/nologin
fi
echo "Completo, configurado o Usuário do Apache: httpd"
