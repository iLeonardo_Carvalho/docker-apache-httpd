# --------------------
# - Configuração httpd
# by iLeonardo Carvalho v1.0
# httpd.conf
# ----------

# -----
# Configurações Externas
LoadModule env_module 			modules/mod_env.so

# -----
# Impede que o Apache mostre sua versão e a versão de seus módulos
ServerTokens Prod
ServerSignature Off

# -----
# Diretório onde esta instalado o apache (configurações)
ServerRoot "/usr/local/apache2"

# -----
# Mutex: Permite que você defina o mecanismo de mutex e o diretório de arquivo
# de mutex para exclusões mútuas individuais, ou alterar os padrões globais
#
# Descomente e altere o diretório se mutexes são baseados em arquivo e o padrão
# diretório de arquivos de mutex não está em um disco local ou não é apropriado
# para algum outro arquivo.
#
# Mutex default:logs
#Mutex file:/var/lock/httpd default

# -----
# Arquivo onde o apache ira guardar o PID
PidFile /var/run/httpd/httpd.pid

# -----
# Timeout das Conexões em segundos
Timeout 300

# -----
# KeepAlive: Se deve ou não permitir conexões persistentes (mais de uma
# solicitação por conexão). Definida como "Off" para desativar.
KeepAlive On

# -----
# MaxKeepAliveRequests: O número máximo de solicitações para permitir durante um
# a conexão persistente. Definido como 0 para permitir uma quantidade ilimitada.
# É recomendável que deixar este número alto, para o máximo desempenho.
MaxKeepAliveRequests 100

# -----
# KeepAliveTimeout: Número de segundos de espera para a próxima solicitação do
# mesmo cliente sobre a mesma conexão.
KeepAliveTimeout 5

# -----
# Listen: Permite que você vincule o Apache para endereços IP específicos e/ou
# portas, em vez do padrão. Veja também o <VirtualHost> directiva.
#
# Mude o listen em específicos endereços IP como mostrado abaixo para
# previnir que o Apache não ligue o endereço IP padrão para todos os <VirtualHost>
#
# - Exemplo:
# Listen IP:PORTA
# listen *:80 (abre a porta 80 em TODOS os IP's que a maquina possuir)
# Listen 127.0.0.1:81 (abre a porta 81 no IP 127.0.0.1)
Listen *:80

# -----
# Conexões seguras (SSL/TLS)
# Nota: Deve estar equivalente compilado estaticamente no /dev/random o mod_ssl


#LoadModule socache_memcache_module 	modules/mod_socache_memcache.so
#LoadModule ssl_module 			modules/mod_ssl.so

#Include conf/extra/httpd-ssl.conf
<IfModule ssl_module>
     SSLRandomSeed startup builtin
     SSLRandomSeed connect builtin
</IfModule>


# -----
# User:Group do Apache2 para leitura dos <VirtualHost>

LoadModule unixd_module 		modules/mod_unixd.so
<IfModule unixd_module>
     User httpd
     Group httpd
</IfModule>

# -----
# ServerAdmin: Seu endereço, onde os problemas com o servidor deve ser enviados
# por Email. Este endereço é exibido em algumas páginas geradas pelo servidor,
# tais como documentos de erro. e.g. admin@your-domain.com
ServerAdmin apache2@localhost

# -----
# ServerName: Dá o nome e a porta que o servidor usa para identificar-se.
# Isso pode muitas vezes ser determinado automaticamente, mas recomendamos que
# você especifique explicitamente para evitar problemas durante a inicialização.
#
# Se seu host não tem um nome DNS registrado, insira seu endereço IP aqui.
#
# - Exemplo:
# ServerName www.exemplo.com:80
ServerName localhost

# -----
# Obter o nome do servidor do Host: cabeçalho <header>
UseCanonicalName Off

# ------------------------[Diretórios]

# -----
# Nega o acesso para a totalidade do sistema de arquivos do seu servidor.
# Explicitamente deve permitir acesso aos diretórios de conteúdo da web em
# outros blocos <Directory> abaixo.
<Directory />
     # Se quiser executar CGI/PERL em todos os diretórios
     # basta adicionar +ExecCGI na TAG Options, ou colocar
     # All para todas as opções
     Options All
     AllowOverride All
     Order deny,allow
     Allow from all
     Satisfy All
</Directory>

# -----
# DocumentRoot: O diretório que você vai servir seus documentos. Por padrão,
# todas as solicitações são tomadas a partir deste diretório, mas aliases e
# links simbólicos podem ser usados para apontar para outros locais.
DocumentRoot "/var/www/html"

<Directory "/var/www/html">
     # Diretive Options e suas configuracoes
     # Indexes -> quando o apache nao tem
     # um arquivo index. O proprio servidor
     # gera o conteudo do diretorio
     # FollowSymlinks -> mostra links simbolicos
     # MultiViews -> suprime extensao de arquivos

     Options All
     #Options Indexes FollowSymLinks Multiviews
     AllowOverride None
     Order allow,deny
     Allow from all
</Directory>

# -----
# /usr/local/apache2/cgi-bin deve ser alterado para seu diretório
# de ScriptAliased CGI existe, se você tiver configurado.
<Directory /usr/local/apache2/cgi-bin>
     AllowOverride None
     Options None
     Order allow,deny
     Allow from all

     Require all granted
</Directory>

# -----
# As linhas seguintes evitar arquivos .htaccess e .htpasswd sejam
# visualizados por clientes Web.
<Files ".ht*">
     Order allow,deny
     Deny from all
     Satisfy All

     Require all denied
</Files>

# -----
# DirectoryIndex: Arquivo que o Apache vai iniciar no diretório
# solicitado por clientes Web.

LoadModule dir_module 			modules/mod_dir.so
<IfModule dir_module>
     DirectoryIndex index.html index.htm index.php
</IfModule>

# ------------------------[Tratamento de LOG's]

# -----
# ErrorLog: O local do arquivo de log de erro.
ErrorLog /var/log/httpd/error.log

# -----
# LogLevel: Controle o número de mensagens registradas para o error_log.
# Os valores possíveis incluem: debug, info, notice, warn, error, crit,
# alert, emerg.
LogLevel warn

# -----
# Log de erros do dominio padrão

LoadModule log_config_module 		modules/mod_log_config.so
LoadModule logio_module 		modules/mod_logio.so
<IfModule log_config_module>
     LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
     LogFormat "%h %l %u %t \"%r\" %>s %b" common

     LogFormat "%{Referer}i -> %U" referer
     LogFormat "%{User-agent}i" agent
 <IfModule logio_module>
   LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
 </IfModule>
     CustomLog /var/log/httpd/access.log common
     #CustomLog /var/log/httpd/access.log combined
</IfModule>

# ------------------------[Módulos]

# -----
# Dynamic Shared Object (DSO) Support
#
# Para ser capaz de usar a funcionalidade de um módulo que foi construído
# como um DSO, você tem que colocar linhas no 'LoadModule' correspondentes
# neste local, então as directivas continham são _before_ realmente disponíveis
# são usados.
#
# - Exemplo:
#LoadModule php5_module 		modules/libphp5.so
## Ativa php se voce compilou o apache com o php

<FilesMatch "\.ph(p[2-5]?|tml|phtm|phtml|asp|aspx)$">
     SetHandler application/x-httpd-php
</FilesMatch>
<FilesMatch "\.phps$">
     SetHandler application/x-httpd-php-source
</FilesMatch>

#LoadModule unique_id_module 		modules/mod_unique_id.so
#Include modules/mod_security2.load
<IfModule security2_module>
     Include conf/modsecurity.conf
     Include conf/crs/modsecurity_crs_10_setup.conf
     Include conf/crs/base_rules/*.conf
</IfModule>
## Ativa ModSecurity

LoadModule authn_file_module 		modules/mod_authn_file.so
#LoadModule authn_dbm_module 		modules/mod_authn_dbm.so
#LoadModule authn_anon_module 		modules/mod_authn_anon.so
#LoadModule authn_dbd_module 		modules/mod_authn_dbd.so
#LoadModule authn_socache_module 	modules/mod_authn_socache.so
LoadModule authn_core_module 		modules/mod_authn_core.so
LoadModule authz_host_module 		modules/mod_authz_host.so
LoadModule authz_groupfile_module 	modules/mod_authz_groupfile.so
LoadModule authz_user_module 		modules/mod_authz_user.so
#LoadModule authz_dbm_module 		modules/mod_authz_dbm.so
#LoadModule authz_owner_module 		modules/mod_authz_owner.so
#LoadModule authz_dbd_module 		modules/mod_authz_dbd.so
LoadModule authz_core_module 		modules/mod_authz_core.so
LoadModule access_compat_module 	modules/mod_access_compat.so
LoadModule auth_basic_module 		modules/mod_auth_basic.so
#LoadModule auth_form_module 		modules/mod_auth_form.so
LoadModule auth_digest_module 		modules/mod_auth_digest.so
#LoadModule allowmethods_module 	modules/mod_allowmethods.so
#LoadModule file_cache_module 		modules/mod_file_cache.so
#LoadModule cache_module 		modules/mod_cache.so
#LoadModule cache_disk_module 		modules/mod_cache_disk.so
#LoadModule cache_socache_module 	modules/mod_cache_socache.so
#LoadModule socache_shmcb_module 	modules/mod_socache_shmcb.so
#LoadModule socache_dbm_module 		modules/mod_socache_dbm.so
#LoadModule macro_module 		modules/mod_macro.so
#LoadModule dbd_module 			modules/mod_dbd.so
#LoadModule dumpio_module 		modules/mod_dumpio.so
#LoadModule buffer_module 		modules/mod_buffer.so
#LoadModule ratelimit_module 		modules/mod_ratelimit.so
LoadModule reqtimeout_module 		modules/mod_reqtimeout.so
#LoadModule ext_filter_module 		modules/mod_ext_filter.so
#LoadModule request_module 		modules/mod_request.so
#LoadModule include_module 		modules/mod_include.so
LoadModule filter_module 		modules/mod_filter.so
#LoadModule substitute_module 		modules/mod_substitute.so
#LoadModule sed_module 			modules/mod_sed.so
#LoadModule deflate_module 		modules/mod_deflate.so
#LoadModule log_debug_module 		modules/mod_log_debug.so
#LoadModule expires_module 		modules/mod_expires.so
#LoadModule usertrack_module 		modules/mod_usertrack.so
LoadModule version_module 		modules/mod_version.so
#LoadModule remoteip_module 		modules/mod_remoteip.so
#LoadModule session_module 		modules/mod_session.so
#LoadModule session_cookie_module 	modules/mod_session_cookie.so
#LoadModule session_dbd_module 		modules/mod_session_dbd.so
#LoadModule slotmem_shm_module 		modules/mod_slotmem_shm.so
#LoadModule lbmethod_byrequests_module 	modules/mod_lbmethod_byrequests.so
#LoadModule lbmethod_bytraffic_module 	modules/mod_lbmethod_bytraffic.so
#LoadModule lbmethod_bybusyness_module 	modules/mod_lbmethod_bybusyness.so
#LoadModule lbmethod_heartbeat_module 	modules/mod_lbmethod_heartbeat.so
LoadModule mpm_event_module 		modules/mod_mpm_event.so
LoadModule status_module 		modules/mod_status.so
LoadModule autoindex_module 		modules/mod_autoindex.so
#LoadModule info_module 		modules/mod_info.so

<IfModule !mpm_prefork_module>
     #LoadModule cgid_module 		modules/mod_cgid.so
</IfModule>
<IfModule mpm_prefork_module>
     #LoadModule cgi_module 		modules/mod_cgi.so
</IfModule>

# -----
# Configura o mod_proxy_html para entender HTML4/XHTML1

#LoadModule proxy_module 		modules/mod_proxy.so
#LoadModule proxy_connect_module 	modules/mod_proxy_connect.so
#LoadModule proxy_ftp_module 		modules/mod_proxy_ftp.so
#LoadModule proxy_http_module 		modules/mod_proxy_http.so
#LoadModule proxy_fcgi_module 		modules/mod_proxy_fcgi.so
#LoadModule proxy_scgi_module 		modules/mod_proxy_scgi.so
#LoadModule proxy_wstunnel_module 	modules/mod_proxy_wstunnel.so
#LoadModule proxy_ajp_module 		modules/mod_proxy_ajp.so
#LoadModule proxy_balancer_module 	modules/mod_proxy_balancer.so
#LoadModule proxy_express_module 	modules/mod_proxy_express.so

<IfModule proxy_html_module>
     Include conf/extra/proxy-html.conf
</IfModule>

#LoadModule dav_module 			modules/mod_dav.so
#LoadModule dav_fs_module 		modules/mod_dav_fs.so
#LoadModule vhost_alias_module 		modules/mod_vhost_alias.so
LoadModule negotiation_module 		modules/mod_negotiation.so
#LoadModule actions_module 		modules/mod_actions.so
#LoadModule speling_module 		modules/mod_speling.so
#LoadModule userdir_module 		modules/mod_userdir.so
LoadModule rewrite_module 		modules/mod_rewrite.so
LoadModule headers_module 		modules/mod_headers.so

LoadModule setenvif_module 		modules/mod_setenvif.so

LoadModule alias_module 		modules/mod_alias.so
<IfModule alias_module>
     # -----
     # Redirect: Permite que você fale clientes documentos que costumavam
     # existir no espaço para nome do seu servidor, mas não mais. O cliente
     # vai fazer uma nova solicitação para o documento em sua nova localização.
     # - Exemplo:
     # Redirect permanent /foo http://www.example.com/bar

     # -----
     # Alias: Mapas de caminhos de web em caminhos de arquivos e é usado para
     # acessar o conteúdo que não vive sob o DocumentRoot.
     # - Exemplo:
     # Alias /webpath /full/filesystem/path

     # -----
     # ScriptAlias: Este controla os diretórios que contêm scripts de servidor
     ScriptAlias /cgi-bin/ /usr/local/apache2/cgi-bin/
</IfModule>

<IfModule cgid_module>
     # -----
     # ScriptSock: Em servidores de threaded, designe o caminho para o socket
     # UNIX usado para se comunicar com o daemon CGI de mod_cgid.
     # - Exemplo:
     # Scriptsock cgisock
</IfModule>


LoadModule mime_module 			modules/mod_mime.so
<IfModule mime_module>
     # -----
     # TypesConfig aponta para o arquivo que contém a lista de mapeamentos de
     # extensão de nome para o MIME-type.
     TypesConfig conf/mime.types

     # -----
     # AddType permite que você adicionar ou substituir o arquivo de configuração
     # MIME especificado em TypesConfig para tipos de arquivo específicos.

     # -----
     # AddEncoding permite que você tenha certos navegadores descompactar informações
     # em tempo real. Nota: Nem todos os navegadores oferecem suporte a isso.

     AddType application/x-compress .Z
     AddType application/x-gzip .gz .tgz
</IfModule>

# -----
# O módulo mod_mime_magic permite o servidor usar várias dicas do conteúdo do arquivo
# em si para determinar o seu tipo. A diretiva MIMEMagicFile diz o módilo onde e se
# situam as definições de dica.
#MIMEMagicFile conf/magic

# -----
# MaxRanges: Máximo número de intervalos em um pedido antes de retornar o recursos
# inteiro, ou um dos valores especiais como 'default', 'none' ou 'ilimitado'.
#
# Por padrão é aceitar 200 ranges.
#MaxRanges unlimited

# -----
# EnableMMAP e EnableSendfile: On systems that support it,
# Em sistemas que suportam isso, mapeamento de memória ou o syscall sendfile pode ser
# utilizado para entregar os arquivos. Isso geralmente melhora o desempenho do servidor,
# mas deve ser desligado quando servindo de sistemas de arquivos montados em rede ou se
# o suporte para essas funções caso contrário quebrado em seu sistema.
#
# Padrão: EnableMMAP On, EnableSendfile Off
#EnableMMAP off
#EnableSendfile on

# ------------------------[Include]

# Server-pool management (MPM specific)
#Include conf/extra/httpd-mpm.conf

# Multi-language error messages
#Include conf/extra/httpd-multilang-errordoc.conf

# Fancy directory listings
Include conf/extra/httpd-autoindex.conf

# Language settings
Include conf/extra/httpd-languages.conf

# User home directories
#Include conf/extra/httpd-userdir.conf

# Real-time info on requests and configuration
#Include conf/extra/httpd-info.conf

# Virtual hosts
#Include conf/extra/httpd-vhosts.conf

# Local access to the Apache HTTP Server Manual
#Include conf/extra/httpd-manual.conf

# Distributed authoring and versioning (WebDAV)
#Include conf/extra/httpd-dav.conf

# Various default settings
#Include conf/extra/httpd-default.conf
